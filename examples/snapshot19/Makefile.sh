#!/bin/bash

# clean generated files
rm -f main-bundle.js

# Depends on having rollup command installed, with the command:
# npm install rollup -g
rollup --format umd --name main --input main.js --output main-bundle.js
