(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
function Breed(breedname) {
  this.name = breedname;
  this.toString = function() {
    return this.name;
  };
}
var $__default = Breed;

//# sourceURL=d:/bitbucket/codecoding/modules_101/examples/snapshot15/breed.js
},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__breed__,
    $__class__;
var Breed = ($__breed__ = require("./breed"), $__breed__ && $__breed__.__esModule && $__breed__ || {default: $__breed__}).default;
var Class = ($__class__ = require("./class"), $__class__ && $__class__.__esModule && $__class__ || {default: $__class__}).default;
function Character(name, classname, breedname) {
  this.name = name;
  this.class = new Class(classname);
  this.breed = new Breed(breedname);
  this.toString = function() {
    return this.name + ', ' + this.breed + ' ' + this.class;
  };
}
var $__default = Character;

//# sourceURL=d:/bitbucket/codecoding/modules_101/examples/snapshot15/character.js
},{"./breed":1,"./class":3}],3:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
function Class(classname) {
  this.name = classname;
  this.toString = function() {
    return this.name;
  };
}
var $__default = Class;

//# sourceURL=d:/bitbucket/codecoding/modules_101/examples/snapshot15/class.js
},{}],4:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var enums = {
  breeds: {
    elf: 'elf',
    dwarf: 'dwarf',
    human: 'human',
    giant: 'giant',
    orc: 'orc',
    troll: 'troll',
    goblin: 'goblin'
  },
  classes: {
    warrior: 'warrior',
    wizard: 'wizard',
    alchemist: 'alchemist',
    artisan: 'artisan',
    farmer: 'farmer',
    cleric: 'cleric'
  }
};
var $__default = enums;

//# sourceURL=d:/bitbucket/codecoding/modules_101/examples/snapshot15/enums.js
},{}],5:[function(require,module,exports){
"use strict";
var $__enums__,
    $__character__;
var enums = ($__enums__ = require("./enums"), $__enums__ && $__enums__.__esModule && $__enums__ || {default: $__enums__}).default;
var Character = ($__character__ = require("./character"), $__character__ && $__character__.__esModule && $__character__ || {default: $__character__}).default;
var char = new Character('Eldelbar', enums.classes.warrior, enums.breeds.elf);
var output = document.getElementById('output');
output.innerHTML = char;

//# sourceURL=d:/bitbucket/codecoding/modules_101/examples/snapshot15/main.js
},{"./character":2,"./enums":4}]},{},[5]);
