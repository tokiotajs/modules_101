define('breed', [], function () {
    return function Breed(breedname) {
        this.name = breedname;
        this.toString = function () {
            return this.name;
        };
    };
});
define('class', [], function () {
    return function Class(classname) {
        this.name = classname;
        this.toString = function () {
            return this.name;
        };
    };
});
define('character', [
    'breed',
    'class'
], function (Breed, Class) {
    return function Character(name, classname, breedname) {
        this.name = name;
        this.class = new Class(classname);
        this.breed = new Breed(breedname);
        this.toString = function () {
            return this.name + ', ' + this.breed + ' ' + this.class;
        };
    };
});
define('enums', [], function () {
    return {
        breeds: {
            elf: 'elf',
            dwarf: 'dwarf',
            human: 'human',
            giant: 'giant',
            orc: 'orc',
            troll: 'troll',
            goblin: 'goblin'
        },
        classes: {
            warrior: 'warrior',
            wizard: 'wizard',
            alchemist: 'alchemist',
            artisan: 'artisan',
            farmer: 'farmer',
            cleric: 'cleric'
        }
    };
});
require([
    './character',
    './enums'
], function (Character, enums) {
    var char = new Character('Eldelbar', enums.classes.warrior, enums.breeds.elf);
    var output = document.getElementById('output');
    output.innerHTML = char;
});
define('main', [
    'character',
    'enums'
], function () {
    return;
});