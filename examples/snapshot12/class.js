define([], function () {
	return function Class(classname) {
		this.name = classname;
		this.toString = function () { return this.name; };
	};
});