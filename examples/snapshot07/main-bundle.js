/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var Character = __webpack_require__(1);
	var enums = __webpack_require__(4);

	var char = new Character('Eldelbar', enums.classes.warrior, enums.breeds.elf);
	var output = document.getElementById('output');
	output.innerHTML = char;

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var Breed = __webpack_require__(2);
	var Class = __webpack_require__(3);

	function Character(name, classname, breedname) {
		this.name = name;
		this.class = new Class(classname);
		this.breed = new Breed(breedname);
		this.toString = function () { return this.name + ', ' + this.breed + ' ' + this.class; };
	};

	module.exports = Character;

/***/ },
/* 2 */
/***/ function(module, exports) {

	function Breed(breedname) {
		this.name = breedname;
		this.toString = function () { return this.name; };
	}

	module.exports = Breed;

/***/ },
/* 3 */
/***/ function(module, exports) {

	function Class(classname) {
		this.name = classname;
		this.toString = function () { return this.name; };
	}

	module.exports = Class;

/***/ },
/* 4 */
/***/ function(module, exports) {

	var enums = { 
		breeds : { elf : 'elf',	dwarf: 'dwarf',	human: 'human', giant: 'giant',	orc: 'orc',	troll: 'troll',	goblin: 'goblin' }, 
		classes : { warrior: 'warrior', wizard: 'wizard', alchemist: 'alchemist', artisan: 'artisan', farmer: 'farmer',	cleric: 'cleric' }
	};

	module.exports = enums;

/***/ }
/******/ ]);