import Breed from './breed';
import Class from './class';

export default function Character(name, classname, breedname) {
	console.log(Breed);
	this.name = name;
	this.class = new Class(classname);
	this.breed = new Breed(breedname);
	this.toString = function () { return this.name + ', ' + this.breed + ' ' + this.class; };
}
