ES6 & Babelify (RequireJs)

 * [ES6 Modules](https://github.com/lukehoban/es6features#modules)
 * [RequireJs-babel](https://github.com/mikach/requirejs-babel)
 * built on [Babel](https://babeljs.io/)
