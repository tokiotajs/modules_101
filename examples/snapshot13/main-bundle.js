(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = function Breed(breedname) {
    this.name = breedname;
    this.toString = function () {
        return this.name;
    };
};
},{}],2:[function(require,module,exports){
var Breed = require('./breed'), Class = require('./class');
module.exports = function Character(name, classname, breedname) {
    this.name = name;
    this.class = new Class(classname);
    this.breed = new Breed(breedname);
    this.toString = function () {
        return this.name + ', ' + this.breed + ' ' + this.class;
    };
};
},{"./breed":1,"./class":3}],3:[function(require,module,exports){
module.exports = function Class(classname) {
    this.name = classname;
    this.toString = function () {
        return this.name;
    };
};
},{}],4:[function(require,module,exports){
module.exports = {
    breeds: {
        elf: 'elf',
        dwarf: 'dwarf',
        human: 'human',
        giant: 'giant',
        orc: 'orc',
        troll: 'troll',
        goblin: 'goblin'
    },
    classes: {
        warrior: 'warrior',
        wizard: 'wizard',
        alchemist: 'alchemist',
        artisan: 'artisan',
        farmer: 'farmer',
        cleric: 'cleric'
    }
};
},{}],5:[function(require,module,exports){
var Character = require('./character'), enums = require('./enums');
var char = new Character('Eldelbar', enums.classes.warrior, enums.breeds.elf);
var output = document.getElementById('output');
output.innerHTML = char;
},{"./character":2,"./enums":4}]},{},[5]);
